package com.trakkies.expenseSheet;

import javafx.util.StringConverter;

public class IntegerStringConverter extends StringConverter<Integer> {

	@Override
	public String toString(Integer value) {
	     // If the specified value is null, return a zero-length String
        if (value == null || value <= 0) {
            return "";
        }
        value = value > 24 ? 24 : value;
        return String.format("%d%%",value);
	}

	@Override
	public Integer fromString(String string) {
        // If the specified value is null or zero-length, return null
        if (string == null) {
            return 0;
        }
        if (string.contains("%"))
        	string = string.trim().substring(0, string.length()-1);
        if (string.length() < 1 || Integer.valueOf(string) <=0) {
            return 0;
        }
        Integer result = Integer.valueOf(string)>23 ? 24 : Integer.valueOf(string);
        return result;
	}

}
