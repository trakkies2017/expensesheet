package com.trakkies.expenseSheet;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.util.Callback;
import javafx.util.StringConverter;


public class SpinnerTableCell<S, T> extends TableCell<S, Integer> {
	private Spinner<Integer> spinner;
	private StringConverter<Integer> converter = null;
	private IntegerSpinnerValueFactory valueFactory;
	private Boolean spinnerEditable;
	public SpinnerTableCell(Boolean editable) {
		super();
		this.converter = new IntegerStringConverter();
		this.valueFactory = new IntegerSpinnerValueFactory(0,24,1);
		this.valueFactory.setConverter(converter);
		this.spinnerEditable = editable;
		this.setAlignment(Pos.CENTER);
	}
	
	 
	@Override
	public void startEdit() {
	
		// Let the ancestor do the plumbing job
		super.startEdit();
		if (this.spinner == null) {
			this.createSpinner();
		}
		this.setGraphic(spinner);
		this.setText(null);
	}
	
	@Override
	public void cancelEdit() {
		super.cancelEdit();
		this.setText(converter.toString(spinner.getValue()));
		this.setGraphic(null);
	}
	
	@Override
	public void updateItem(Integer item, boolean empty) {
		super.updateItem(item, empty);
	 
	// Take actions based on whether the cell is being edited or not
		if (empty) {
				this.setText(null);
		} else {
			if (this.isEditing()) {
				if (spinner != null) {
					spinner.getValueFactory().setValue((Integer)item);
				}
				this.setText(null);
				this.setGraphic(spinner);
			} else {
				this.setText(converter.toString(item));
				this.setGraphic(null);
			}
		}
	}
	
	@Override
	public void commitEdit(Integer value) {
		super.commitEdit(value);
		this.setText(converter.toString(value));
		this.setGraphic(null);
	}
	
	private void createSpinner() {
		spinner = new Spinner<Integer>();
		spinner.setValueFactory(valueFactory);
		// Set the current value in the cell to the Spinner value
		spinner.getValueFactory().setValue((Integer)this.getItem());
		spinner.setEditable(spinnerEditable);
		spinner.setValueFactory(valueFactory);

		// Configure the Spinner properties
		spinner.setPrefWidth(this.getWidth() - this.getGraphicTextGap() * 2);
		spinner.setEditable(this.spinnerEditable);
		// Commit the new value when the user selects or enters an integer
		spinner.valueProperty().addListener(new ChangeListener<Integer>() {
			@Override
			public void changed(ObservableValue<? extends Integer> prop,
					Integer oldValue,
					Integer newValue) {
				if (SpinnerTableCell.this.isEditing()) {
					SpinnerTableCell.this.updateItem(
							newValue, newValue == null);
				}
			}
		});
	}
		 
/*	private void addListenerKeyChange() {
        spinner.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            commitEditorText();
        });
    }
	
	private void commitEditorText() {
        if (!isEditable()) return;
        String text = spinner.getEditor().getText();
        SpinnerValueFactory<T> valueFactory = getValueFactory();
        if (valueFactory != null) {
            StringConverter<T> converter = valueFactory.getConverter();
            if (converter != null) {
                T value = converter.fromString(text);
                valueFactory.setValue(value);
            }
        }
    }*/
	
		public static <S> Callback<TableColumn<S, Integer>,
				TableCell<S, Integer>> forTableColumn() {
			return forTableColumn(true);
		}
		 
		public static <S> Callback<TableColumn<S, Integer>,
				TableCell<S, Integer>> forTableColumn(boolean
						spinnerEditable) {
			return (col -> new SpinnerTableCell<>(spinnerEditable));
		}
}		 
/*		public static <S> Callback<TableColumn<S, Integer>, TableCell<S,
			Integer>> forTableColumn(StringConverter<Integer> converter) {
			return forTableColumn(converter, true);
		}
		 
		public static <S> Callback<TableColumn<S, Integer>, TableCell<S,
		Integer>> forTableColumn(StringConverter<Integer> converter,
					boolean spinnerEditable) {
			return (col -> new SpinnerTableCell<>(converter, spinnerEditable));
		}}*/