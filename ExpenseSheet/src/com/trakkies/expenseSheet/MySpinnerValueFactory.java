package com.trakkies.expenseSheet;

import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;

public class MySpinnerValueFactory extends IntegerSpinnerValueFactory {

	public MySpinnerValueFactory(int min, int max, int initial) {
		super(min, max,initial);
		this.setConverter(new IntegerStringConverter());
	}
	
	
}
