package com.trakkies.expenseSheet;
import javafx.scene.control.TableCell;


public class AmountExcTableCell<S,T> extends TableCell<ExpenseItem, Double> {
	private Double oldValue;
	private String columnId;
	public AmountExcTableCell() {
	}

	@Override
	public void startEdit() {
		if (!(isEditable() && getTableView().isEditable() && getTableColumn().isEditable())) {
			return;
		}
		oldValue = Double.valueOf(this.getText());
		columnId = this.getTableColumn().idProperty().get();
		super.startEdit();
	}
	
	@Override
	public void cancelEdit() {
		this.setText(String.format("%.2f€",oldValue.doubleValue()));
		updateAmounts(oldValue);
		
	}
	
	@Override
	public void updateItem(Double amount, boolean empty) {
		this.setText(String.format("%.2f€",amount.doubleValue()));
	}
	@Override
	public void commitEdit(Double amount) {
		if (amount == 0.0) 
			this.setText(null);
		else
			this.setText(String.format("%.2f€",amount.doubleValue()));
	}

	private void updateAmounts(Double value){
		if (this.columnId.equals("Excl")) {
			this.getTableRow();
		}
	}
}
