package com.trakkies.expenseSheet;
/**
 * 
 */
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;

/**
 * @author Marc
 *
 */
public class ExpenseItem {
	private final ReadOnlyIntegerWrapper itemWrapper;
	private final StringProperty description ;
	private final StringProperty project ;				
	private final ObjectProperty<LocalDate> expDate;
	private final IntegerProperty  vat;
	private final DoubleProperty  amountExc;
	private final DoubleProperty  amountVat;
	private final DoubleProperty  amountInc;

	
	private static AtomicInteger itemSequence = new AtomicInteger(0);
	
	
	public ExpenseItem() {
		itemWrapper = new ReadOnlyIntegerWrapper(this,"Item:",itemSequence.incrementAndGet());
		description = new SimpleStringProperty(this,"description:",null);
		project = new SimpleStringProperty(this,"project:",null);
		expDate = new SimpleObjectProperty<>(this,"Date of receipt",null);
		vat = new SimpleIntegerProperty(this,"VAT(btw) percetage",21);
		amountExc = new SimpleDoubleProperty(this,"amount VAT excluded");
		amountVat = new SimpleDoubleProperty(this,"VAT");
		amountInc = new SimpleDoubleProperty(this,"amount VAT included");
	}
	
	/* Item property */
	public final int getItem(){
		return itemWrapper.get();
	}
	
	public final ReadOnlyIntegerProperty itemProperty() {
		return itemWrapper.getReadOnlyProperty();
	}
	
	/* Description property */
	public final String getDescription(){
		return description.get();
	}
		
	public final void setDescription(String descr){
		descriptionProperty().set(descr);
	}
	
	public final StringProperty descriptionProperty(){
		return description;
	}
	
	/* Project property */
	public final String getProject(){
		return project.get();
	}
		
	public final void setProject(String project){
		projectProperty().set(project);
	}
	
	public final StringProperty projectProperty(){
		return project;
	}
	
	
	/* Date property */
	public final LocalDate getExpDate(){
		return expDate.get();
	}
		
	public final void setExpDate(LocalDate expDate){
		expDateProperty().set(expDate);
	}
	
	public final ObjectProperty<LocalDate> expDateProperty() {
		return expDate;
	}
	
	/* VAT Property */
	
	public final int getVat(){
		return vat.get();
	}
		
	public final void setVat(int newVat){
		vatProperty().set(newVat);
	}
	
	public final IntegerProperty vatProperty() {
		return vat;
	}


	
	/* Amount Excluded Property */
	
	public final double getAmountExc(){
		return amountExc.get();
	}
		
	private final void setAmountExcDirect(double newAmount){
		
		amountExcProperty().set(newAmount);
	}
	
	public final DoubleProperty amountExcProperty() {
		return amountExc;
	}

	public final void setAmountExc(double newAmount) {
		double amountInclVat = (double) (newAmount * (1+getVat()/100.0));
		setAmountIncDirect(amountInclVat);
		setAmountVat(newAmount*(double)(getVat()/100.0));
		amountExcProperty().set(newAmount);
	}

	/* Item property */
	public final double getAmountVat(){
		return amountVat.get();
	}
	
	public final void setAmountVat(double newAmount){
		amountVatProperty().set(newAmount);
	}
	
	public final DoubleProperty amountVatProperty() {
		return amountVat;
	}

	
	/* Amount Included Property */
	
	public final double getAmountInc(){
		return amountExc.get();
	}
		
	public final void setAmountInc(double newAmount){
		double amountExclVat = (double) (newAmount / (1+getVat()/100.0));
		setAmountExcDirect(amountExclVat);
		setAmountVat(amountExclVat*(double)(getVat()/100.0));
		amountIncProperty().set(newAmount);
	}
	
	public final DoubleProperty amountIncProperty() {
		return amountInc;
	}
	
	private final void setAmountIncDirect(double newAmount) {
		amountIncProperty().set(newAmount);
	}
	/* Domain specific Business Rules*/
	public boolean isValidExpenseDate(LocalDate expDate) {
		return isValidExpenseDate(expDate, new ArrayList<>());
	}
	
	public boolean isValidExpenseDate(LocalDate expDate, List<String> errorList) {
		if (expDate == null) {
			errorList.add("a date must be supplied.");
			return false;
		} else if (expDate.isAfter(LocalDate.now())) {
			errorList.add("the date can not be in the future.");
			return false;
		}
		return true;
	}
	
	public boolean isValidExpense(List<String> errorList) {
		return isValidExpense(this, errorList);
	}
	
	public boolean isValidExpense(ExpenseItem e, List<String> errorList) {
		boolean isValid = true;
		
		String desc = e.description.get();
		if (desc == null || desc.trim().length() == 0) {
			errorList.add("description is missing");
			isValid = false;
		}

		int vat = e.vat.get();
		if (vat < 0) {
			errorList.add("Vat percentage can not be negative");
			isValid = false;
		}
		if (vat > 24) {
			errorList.add("Vat percentage too big");
			isValid = false;
		}
		
		if (!isValidExpenseDate(e.expDate.get(), errorList)) {
			isValid = false;
		}

		return isValid;
	}
	
	public void clear() {
		this.descriptionProperty().set("");
		this.expDateProperty().set(null);
		this.projectProperty().set(null);
		this.vatProperty().set(21);
		this.amountExcProperty().set(0.0);
		this.amountVatProperty().set(0.0);
		this.amountIncProperty().set(0.0);
	}
}
