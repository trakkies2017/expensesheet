package com.trakkies.expenseSheet;

import javafx.util.StringConverter;

/**
 * <p>{@link StringConverter} implementation for {@link Double}
 * (and double primitive) values., modified by Marc</p>
 * @since JavaFX 2.1
 */
public class DoubleStringConverter extends StringConverter<Double> {
    /** {@inheritDoc} */
    @Override public Double fromString(String value) {
        // If the specified value is null or zero-length, return null
        if (value == null) {
            return null;
        }

        value = value.trim();
        if (value.endsWith("€")) {
        	value = value.substring(0, value.length()-2);
        }
        if (value.length() < 1) {
            return null;
        }
        value = value.replace(',','.');
        return Double.valueOf(value);
    }

    /** {@inheritDoc} */
    @Override public String toString(Double value) {
        // If the specified value is null, return a zero-length String
        if (value == null || value.doubleValue() == 0) {
            return "";
        }
        
        return String.format("%.2f€",value.doubleValue());
    }
}
