package com.trakkies.expenseSheet;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.beans.binding.NumberBinding;
import javafx.scene.*;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import org.apache.commons.validator.routines.checkdigit.IBANCheckDigit;
/**
 * 
 */

/**
 * @author Marc
 *
 */
public class ExpenseSheet extends Application {
	private boolean nameOk = false;
	private boolean ibanOk = false;
	private boolean dateOk = false;
	private IBANCheckDigit ibanCheckDigit = new IBANCheckDigit();
	private DatePicker datePicker = new DatePicker();
	private Group nameBox = new Group();	
	private static TextField nameFld,ibanFld;
	private ExpenseTable table;
	private VBox body;
	/* 
	 * 
	 */
	@Override
	public void start(final Stage primaryStage) throws Exception {
		
		final Label nameLbl = new Label("Name:");
		nameFld = new TextField();
		final Label ibanLbl = new Label("IBAN:");
		ibanFld = new TextField();
		final Label dateLbl = new Label("Submission Date:");
		nameFld.setPrefColumnCount(20);
		ibanFld.setPrefColumnCount(20);
		Image imgOk = new Image("res/okCheck.png",20,20,true,true);
		Image imgNo = new Image("res/unCheck.png",20,20,true,true);
		ImageView checkName = new ImageView(imgNo);
		ImageView checkIban = new ImageView(imgNo);
		nameLbl.setLayoutX(10);
		nameLbl.setLayoutY(20);
		NumberBinding layoutXBinding = nameLbl.layoutXProperty().add(dateLbl.widthProperty().add(10));
		nameFld.layoutXProperty().bind(layoutXBinding);
		nameFld.layoutYProperty().bind(nameLbl.layoutYProperty());
		NumberBinding layoutXIconBinding = nameFld.layoutXProperty().add(ibanFld.widthProperty().add(10));
		checkName.layoutXProperty().bind(layoutXIconBinding);
		checkName.layoutYProperty().bind(nameLbl.layoutYProperty());
		NumberBinding layoutIbanYBinding = nameLbl.layoutYProperty().add(nameLbl.heightProperty().add(10));
		ibanLbl.setLayoutX(10);
		ibanLbl.layoutYProperty().bind(layoutIbanYBinding);
		ibanFld.layoutXProperty().bind(layoutXBinding);
		ibanFld.layoutYProperty().bind(ibanLbl.layoutYProperty());
		checkIban.layoutXProperty().bind(layoutXIconBinding);
		checkIban.layoutYProperty().bind(ibanLbl.layoutYProperty());

		NumberBinding layoutDateYBinding = ibanLbl.layoutYProperty().add(ibanLbl.heightProperty().add(10));
		dateLbl.setLayoutX(10);
		dateLbl.layoutYProperty().bind(layoutDateYBinding);
		datePicker.layoutXProperty().bind(layoutXBinding);
		datePicker.layoutYProperty().bind(dateLbl.layoutYProperty());
		nameFld.focusedProperty().addListener((arg0, oldValue,newValue) -> {
			if (nameFld.getText().isEmpty()){
				nameOk = false;
				checkName.setImage(imgNo);
			} else {
				nameOk = true;
				checkName.setImage(imgOk);
			}
		});

		ibanFld.focusedProperty().addListener((arg0, oldValue,newValue) -> {
			if(ibanCheckDigit.isValid(ibanFld.getText())) {
				ibanOk = false;
				checkIban.setImage(imgOk);
			} else {
				ibanOk = true;
				checkIban.setImage(imgNo);
			}
		});
		datePicker.isEditable();
//		dateFld.focusedProperty();
		nameBox.getChildren().addAll(nameLbl,nameFld,checkName,ibanLbl,ibanFld,checkIban,dateLbl,datePicker);
		table = new ExpenseTable(12);

		body = new VBox();
		body.getChildren().addAll(nameBox,table);
		BorderPane root = new BorderPane();
		MenuBuilder menu = new MenuBuilder(this);
		root.setTop(menu.getMenuBar());
		root.setCenter(body);
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("/res/application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public void clearAll() {
		table.clearTable();
	}
	
	
	public Node getBody() {
		return body;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
