package com.trakkies.expenseSheet;
import java.time.LocalDate;

import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
public class ExpenseSheetUtil {
	
	public static TableColumn<ExpenseItem, Integer> getItemColumn() {
		TableColumn<ExpenseItem, Integer> itemCol = new TableColumn<>("Item"); 
		itemCol.setPrefWidth(40);
		itemCol.setSortable(false);
		itemCol.setResizable(false);
		itemCol.setEditable(false);
		itemCol.setStyle("-fx-alignment: CENTER");
		itemCol.setCellValueFactory(new PropertyValueFactory<>("item"));
		return itemCol;
	}
	
	public static TableColumn<ExpenseItem, String> getDescriptionColumn() {
		TableColumn<ExpenseItem, String> descCol = new TableColumn<>("Description");
		descCol.setSortable(false);
		descCol.setPrefWidth(600);
		descCol.setCellFactory(TextFieldTableCell.<ExpenseItem>forTableColumn());
		return descCol;
	}
	
	public static TableColumn<ExpenseItem, String> getProjectColumn() {
		TableColumn<ExpenseItem, String> projCol = new TableColumn<>("Project");
		projCol.setSortable(false);
		projCol.setPrefWidth(100);
		projCol.setCellFactory(TextFieldTableCell.<ExpenseItem>forTableColumn());
		return projCol;
	}

	public static TableColumn<ExpenseItem, LocalDate> getDateColumn() {
		TableColumn<ExpenseItem, LocalDate> dateCol = new TableColumn<>("Date"); 
		dateCol.setCellFactory(DatePickerTableCell.<ExpenseItem>forTableColumn());
		dateCol.setPrefWidth(100);
		dateCol.setSortable(false);
		dateCol.setResizable(false);
		dateCol.setCellValueFactory(new PropertyValueFactory<>("expDate"));
		return dateCol;
	}

	public static TableColumn<ExpenseItem, Integer> getVatPercentageColumn() {
		TableColumn<ExpenseItem, Integer> vatPCol = new TableColumn<>("VAT(btw)\nPerc."); 
		vatPCol.setCellValueFactory(new PropertyValueFactory<>("vat"));
		vatPCol.setCellFactory(SpinnerTableCell.<ExpenseItem>forTableColumn(true));
		vatPCol.setId("Vat");
		vatPCol.setSortable(false);
		vatPCol.setResizable(false);
		return vatPCol;
	}
	
	private static void amountExcOnEditCommit(Event e) {
		@SuppressWarnings("unchecked")
		TableColumn.CellEditEvent<ExpenseItem, Double> ce = (TableColumn.CellEditEvent<ExpenseItem, Double>) e;
		ExpenseItem expense = ce.getRowValue();
		expense.setAmountExc(ce.getNewValue());
	}
	public static TableColumn<ExpenseItem, Double> getAmountExcColumn() {
		TableColumn<ExpenseItem, Double> amountExcCol = new TableColumn<>("Amount\nExcl. VAT"); 
		amountExcCol.setCellValueFactory(new PropertyValueFactory<>("amountExc"));
		amountExcCol.setPrefWidth(150);
		amountExcCol.setSortable(false);
		amountExcCol.setResizable(false);
		amountExcCol.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		amountExcCol.setOnEditCommit(e->amountExcOnEditCommit(e));
		return amountExcCol;
	}

	public static TableColumn<ExpenseItem, Double> getAmountVatColumn() {
		TableColumn<ExpenseItem, Double> amountVatCol = new TableColumn<>("VAT"); 
		amountVatCol.setCellValueFactory(new PropertyValueFactory<>("amountVat"));
		amountVatCol.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		amountVatCol.setPrefWidth(150);
		amountVatCol.setSortable(false);
		amountVatCol.setResizable(false);
		amountVatCol.setEditable(false);
		return amountVatCol;
	}

	private static void amountIncOnEditCommit(Event e) {
		@SuppressWarnings("unchecked")
		TableColumn.CellEditEvent<ExpenseItem, Double> ce = (TableColumn.CellEditEvent<ExpenseItem, Double>) e;
		ExpenseItem expense = ce.getRowValue();
		expense.setAmountInc(ce.getNewValue());
	}	
	public static TableColumn<ExpenseItem, Double> getAmountIncColumn() {
		TableColumn<ExpenseItem, Double> amountIncCol = new TableColumn<>("Amount\nIncl. VAT"); 
		amountIncCol.setCellValueFactory(new PropertyValueFactory<>("amountInc"));
		amountIncCol.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		amountIncCol.setOnEditCommit(e->amountIncOnEditCommit(e));
		amountIncCol.setPrefWidth(150);
		amountIncCol.setSortable(false);
		amountIncCol.setResizable(false);
		return amountIncCol;
	}

}
