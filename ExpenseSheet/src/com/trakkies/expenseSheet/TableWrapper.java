package com.trakkies.expenseSheet;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "complete")
public class TableWrapper {
	private List<ExpenseItem>  table;
	
	@XmlElement(name = "item")
    public List<ExpenseItem> getTable() {
        return table;
    }

		
}
