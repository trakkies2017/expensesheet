package com.trakkies.expenseSheet;

import java.util.Iterator;
import java.util.NoSuchElementException;

import javafx.scene.control.TableView;



/**
 * 
 */

/**
 * @author Marc Emanuel
 *
 */
public class ExpenseTable extends TableView<ExpenseItem> {
		
	public ExpenseTable(Integer nrRows) {
		super();
		for (int i =0; i<nrRows ; i++) this.getItems().add(new ExpenseItem());
		this.setEditable(true);
		this.setFixedCellSize(24.0);
		this.getColumns().addAll(ExpenseSheetUtil.getItemColumn(), 
				ExpenseSheetUtil.getDescriptionColumn(),
				ExpenseSheetUtil.getProjectColumn(),
				ExpenseSheetUtil.getDateColumn(),
				ExpenseSheetUtil.getVatPercentageColumn(),
				ExpenseSheetUtil.getAmountExcColumn(),
				ExpenseSheetUtil.getAmountVatColumn(), 
				ExpenseSheetUtil.getAmountIncColumn());
		this.setSortPolicy(null);
		
	}
	
	public void clearTable() {
		Iterator<ExpenseItem> rowIterator =  this.getItems().iterator();
		try	{for(;;)
			rowIterator.next().clear();
		} catch (NoSuchElementException e){
			return;
		}
	}
	
}
