package com.trakkies.expenseSheet;
import java.io.File;

import javafx.collections.ObservableSet;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javafx.scene.transform.Scale;


public class MenuBuilder {
	private MenuBar menuBar = new MenuBar();
	private Node body;
	
	MenuBuilder(ExpenseSheet expensesheet) {
		this.body = expensesheet.getBody();
		MenuItem itemNew = new MenuItem("New", new ImageView(new Image("/res/document-new.png")));
		itemNew.setOnAction( e-> expensesheet.clearAll());
		MenuItem itemOpen = new MenuItem("Open...", new ImageView(new Image("/res/document-open.png")));
		itemOpen.setOnAction(e -> openDocument());
		MenuItem itemSave = new MenuItem("Save", new ImageView(new Image("/res/document-save.png")));
		MenuItem itemSaveAs = new MenuItem("Save as...", new ImageView(new Image("/res/document-save-as.png")));
		MenuItem itemPrint = new MenuItem("Print", new ImageView(new Image("/res/document-print.png")));
		itemPrint.setOnAction(e->printDocument());
		
		MenuItem itemMail = new MenuItem("Mail...", new ImageView(new Image("/res/mail-message-new.png")));
		MenuItem itemExit = new MenuItem("Exit", new ImageView(new Image("/res/application-exit.png")));
		Menu menuFile = new Menu("File");
		menuFile.getItems().addAll(itemNew,itemOpen,itemSave,itemSaveAs,itemPrint,itemMail,itemExit);
		MenuItem itemCut = new MenuItem("Cut");
		MenuItem itemCopy = new MenuItem("Copy");
		MenuItem itemPaste = new MenuItem("Paste");
		Menu menuEdit = new Menu("Edit");
		menuEdit.getItems().addAll(itemCut, itemCopy, itemPaste);
		menuBar.getMenus().addAll(menuFile, menuEdit);
		
	}
	
	private void openDocument() {
		final FileChooser fileChooser = new FileChooser();
		Stage dialogStage = new Stage(StageStyle.UNIFIED);
		File file = fileChooser.showOpenDialog(dialogStage);
        if (file != null) {
        	//TODO  file open read as csv, calc, excell
        }
            
	}
	
	private void printDocument() {
		ObservableSet<Printer> print = Printer.getAllPrinters();
		if (print == null) return;
		Printer printer = Printer.getDefaultPrinter();
		if (printer == null) printer = print.iterator().next();
		PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, Printer.MarginType.DEFAULT);
	    double scaleX = pageLayout.getPrintableWidth() / body.getBoundsInParent().getWidth();
		Stage dialogStage = new Stage(StageStyle.UNIFIED);
		PrinterJob job = PrinterJob.createPrinterJob(printer);
		if (job != null) {
			boolean showDialog = job.showPrintDialog(dialogStage);
			if(showDialog) {
				body.getTransforms().add(new Scale(scaleX, scaleX));
				boolean success = job.printPage(pageLayout,body);
				body.getTransforms().clear();
				if (success) {
					job.endJob();
				}
			}
		}
	}
	
	public MenuBar getMenuBar(){
		return menuBar;
	}
}
